import React from 'react';
import './App.css';
import PokemonList from './components/PokemonList';

function App() {
	return (
		<div className='app'>
			<PokemonList />
		</div>
	);
}

export default App;
