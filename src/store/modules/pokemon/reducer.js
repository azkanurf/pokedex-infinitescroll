import {GET_POKEMON_LIST, GET_POKEMON_DETAIL} from '../action-types';

const initialState = {
	list: [],
	pokemonDetail: {}
};

const reducer = (state = initialState, action) => {
	// console.log(action.payload);
	switch (action.type) {
		case GET_POKEMON_LIST:
			return {...state, list: action.payload};
		case GET_POKEMON_DETAIL:
			return {...state, pokemonDetail: action.payload};

		default:
			return state;
	}
};

export default reducer;
