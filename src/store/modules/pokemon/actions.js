import {GET_POKEMON_LIST, GET_POKEMON_DETAIL, LOADING, POKEMON_SUCCESS} from '../action-types';
import axios from 'axios';

export const getPokemonList = payload => async dispatch => {
	await dispatch({type: LOADING});
	const {url} = payload;

	await axios({
		method: 'GET',
		url: url
	})
		.then(response => {
			const {data} = response;
			console.log(data);
			dispatch({type: POKEMON_SUCCESS});
			return dispatch({type: GET_POKEMON_LIST, payload: data});
		})
		.catch(error => {
			console.error(error);
			return dispatch({type: POKEMON_SUCCESS});
		});
};

export const getPokemonDetail = ({url}) => async dispatch => {
	await dispatch({type: LOADING});

	console.log(url);
	await axios({
		method: 'GET',
		url: url
	})
		.then(response => {
			const {data} = response;
			console.log(data);
			dispatch({type: POKEMON_SUCCESS});
			return dispatch({type: GET_POKEMON_DETAIL, payload: data});
		})
		.catch(error => {
			console.error(error);
			return dispatch({type: POKEMON_SUCCESS});
		});
};
