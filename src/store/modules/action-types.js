export const LOADING = 'LOADING';
export const POKEMON_SUCCESS = 'POKEMON_SUCCESS';
export const GET_POKEMON_LIST = 'GET_POKEMON_LIST';
export const GET_POKEMON_DETAIL = 'GET_POKEMON_DETAIL';
