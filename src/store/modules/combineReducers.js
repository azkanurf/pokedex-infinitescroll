import {combineReducers} from 'redux';
import pokemonReducer from './pokemon/reducer';
import loadingReducer from './isLoading/reducer';

export default combineReducers({
	pokemon: pokemonReducer,
	loading: loadingReducer
});
