import {LOADING} from '../action-types';

const initialState = false;

const reducer = (state = initialState, action) => {
	if (action.type === LOADING) {
		return true;
	} else if (action.type.includes('SUCCESS')) {
		return false;
	} else if (action.type.includes('FAILED')) {
		return false;
	}

	return state;
};

export default reducer;
