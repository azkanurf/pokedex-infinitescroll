import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import reducer from './modules/combineReducers';

export default function configureStore() {
	return createStore(reducer, applyMiddleware(reduxThunk));
}
