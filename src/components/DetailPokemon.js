import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Loading from './Loading';

export default function DetailPokemonModal(props) {
	const loading = useSelector(state => state.loading);
	const pokemon = useSelector(state => state.pokemon);
	const {pokemonDetail} = pokemon;
	const {
		id,
		name,
		abilities,
		height,
		weight,
		types,
		stats,
		moves,
		base_experience,
		sprites: {front_default}
	} = pokemonDetail;

	return (
		<div className='modal-wrapper'>
			<div className='modal modal_block'>
				<div className='modal-content'>
					<button className='button-close' aria-label='Close' onClick={props.close}>
						<span aria-hidden='true'>×</span>
					</button>
					<div className='header-info'>
						<Loading isLoading={loading} />
						<div>
							<h1 id='id-pk'>#{id}</h1>
							<img src={front_default} />
						</div>
						<div>
							<h1 className='name'>{name}</h1>

							<p>
								<strong>base experience :</strong> {base_experience}
							</p>
							<p>
								<strong>height :</strong> {height}
							</p>
							<p>
								<strong>weight :</strong> {weight}
							</p>
						</div>
					</div>
					<div className='additional-info' style={{display: 'flex'}}>
						<div className='col1'>
							<div>
								<h4 className='name'>Stats</h4>
								{stats.map((el, index) => {
									const {
										base_stat,
										stat: {name: statName}
									} = el;

									return (
										<div key={index}>
											{statName} &nbsp;&nbsp;<span id='base-stat'>{base_stat}</span>
										</div>
									);
								})}
							</div>
							<div>
								<h4 className='name'>Types</h4>
								{types.map((el, index) => {
									const {
										type: {name: typeName}
									} = el;

									return <div key={index}> {typeName}</div>;
								})}
							</div>
							<div>
								<h4 className='name'>Abilities</h4>
								{abilities.map((el, index) => {
									const {
										ability: {name: abilityName}
									} = el;

									return <div key={index}>{abilityName}</div>;
								})}
							</div>
						</div>

						<div className='col2'>
							<h4 className='name'>Moves</h4>
							{moves.map((el, index) => {
								const {
									move: {name: moveName}
								} = el;

								return <div key={index}> {moveName}</div>;
							})}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
