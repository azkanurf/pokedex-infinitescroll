import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';

import {getPokemonList, getPokemonDetail} from '../store/modules/pokemon/actions';

import Loading from './Loading';
import DetailPokemonModal from './DetailPokemon';

export default function PokemonList() {
	const baseUrl = 'https://pokeapi.co/api/v2/pokemon?limit=30&offset=0';
	const dispatch = useDispatch();
	const loading = useSelector(state => state.loading);
	const pokemon = useSelector(state => state.pokemon);
	const {
		list,
		list: {results, next: nextUrl}
	} = pokemon;

	const [listPokemon, setListPokemon] = useState([]);
	const [detailPokemon, setDetailPokemon] = useState(false);

	useEffect(() => {
		//initial fetch
		dispatch(getPokemonList({url: baseUrl}));
	}, [true]);

	useEffect(() => {
		if (list.results) {
			setListPokemon(prevResults => {
				return [...prevResults, ...results];
			});
		}
	}, [pokemon]);

	const handleDetail = async url => {
		await dispatch(getPokemonDetail({url: url}));
		setDetailPokemon(!detailPokemon);
	};

	const closeModal = () => {
		setDetailPokemon(false);
	};

	const fetchMoreData = async () => {
		await dispatch(getPokemonList({url: nextUrl}));
	};

	const renderPokemon = () => {
		return (
			<InfiniteScroll initialLoad={false} loadMore={fetchMoreData} hasMore={true} loader={<Loading isLoading={!loading} />}>
				<div className='listpk-wrapper'>
					{listPokemon.map((el, index) => {
						const {name, url} = el;
						const id = url.split('/')[6];

						return (
							<div className='pokemon' key={index}>
								<img
									src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
									alt='front-default'
									onClick={() => handleDetail(url)}
								/>
								<div className='pk-name'>{name}</div>
							</div>
						);
					})}
				</div>
			</InfiniteScroll>
		);
	};

	return (
		<div>
			<Loading isLoading={loading} />
			{list.results ? renderPokemon() : <> </>}

			{detailPokemon ? <DetailPokemonModal modal={detailPokemon} close={closeModal} /> : <></>}
		</div>
	);
}
