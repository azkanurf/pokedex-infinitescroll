import React from 'react';
import PropTypes from 'prop-types';

const Loading = ({isLoading}) => {
	console.log(isLoading);
	return <>{isLoading ? <div className='loading'></div> : null}</>;
};

Loading.propTypes = {
	isLoading: PropTypes.bool.isRequired
};

Loading.defaultProps = {
	isLoading: false
};

export default Loading;
